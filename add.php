<!doctype html>


<?php require_once 'etc/config.php'; ?>


<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MYtask</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body
<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    <!-- Your menu or Off-canvas content goes here -->
  </div>
  <div class="off-canvas-content" data-off-canvas-content>
    <header>
      <div class="title-bar">
        <div class="title-bar-left">
          <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
          <a href="index.php"><span class="title-bar-title">Task List</span></a>
          <a href=""><img class="title-bar-img" src="img/header.jpeg"></a>
        </div>
      </header>

      <div class="addcontainer">
        <main class="add">
          <form action="insert.php" method="post">

            <div class="description">
              <h3>Descritpion</h3>
              <input type="text" id="description" name="description" placeholder="Description">
            </div>


            <div class="created_by">
              <h3>Created_by</h3>
              <input type="text" id="created_by" name="created_by" placeholder="Created_by">
            </div>

            <div class="due_at">
              <h3>Due At</h3>
              <input type="text" id="due_at" name="due_at" placeholder="dd.mm.yy">
            </div>


            <div class="assigned_to">
              <h3>Assigned to</h3>
              <input type="text" id="assigned_to" name="assigned_to" placeholder="Assigned to">
            </div>

            <div class="priority">
              <h3>Priority</h3>
              <input type="text" id="priority" name="priority" placeholder="Priority">
            </div>

            <div class="status">
              <h3>Status</h3>
              <input type="text" id="status" name="status" placeholder="Status">
            </div>

            <div class="done_by">
              <h3>Done by</h3>
              <input type="text" id="done_by" name="done_by" placeholder="Done by">
            </div>


            <div class="submit">
              <input  type="submit" value="Submit">
            </div>
          </form>
        </main>
      </div>


      <footer>
        <div class="small-12 large-12 columns">
          <div class="copyrights">
            <p>&copy; Copyright 2017 Jakov Ivankovic</p>
          </div>
        </div>
      </footer>
    </div>
  </div>

  <script src="bower_components/jquery/dist/jquery.js"></script>
  <script src="bower_components/what-input/dist/what-input.js"></script>
  <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
  <script src="js/app.js"></script>
</body>
