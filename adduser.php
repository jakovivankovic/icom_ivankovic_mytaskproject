
<?php require_once 'etc/config.php'; ?>


<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MYtask</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body
<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    <ul class="menu vertical">
      <li><a href="personal.php">Personal Presentation</a></li>
      <li><a href="about.php">About</a></li>
      <li><a href="manual.php">Manual</a></li>
      <li><a href="adduser.php">Add User</a></li>
      <li><a href="logout.php">Logout</a></li>
      <br><br>
      <li>Order By</li>
      <li><a data-filter="id">id</a></li>
      <li><a data-filter="status">status</a></li>
    </ul>
  </div>

  <div class="off-canvas-content" data-off-canvas-content>
    <header>
      <div class="title-bar">
        <div class="title-bar-left">
          <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
          <a href="index.php"><span class="title-bar-title">Task List</span></a>
          <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><img class="title-bar-img" src="img/header.jpeg"></button>
        </div>
      </div>
    </header>

      <div class="addusercontainer">
        <main class="add">
          <form action="insertuser.php" method="post">
            <div class="name">
              <h2>Name</h2>
              <input type="text" id="name" name="name" placeholder="Name" required>
            </div>

            <div class="email">
              <h2>Email</h2>
              <input type="text" id="email" name="email" placeholder="Email" autocomplete="off" required>
            </div>

            <div class="password">
              <h2>Password</h2>
              <input type="text" id="password" name="password" placeholder="Password" autocomplete="off" required>
            </div>

            <div class="submit">
              <input  type="submit" value="Submit">
            </div>
          </form>
        </main>
      </div>

      <footer>
        <div class="small-12 large-12 columns">
          <div class="copyrights">
            <p>&copy; Copyright 2017 Jakov Ivankovic</p>
          </div>
        </div>
      </footer>
    </div>
  </div>

  <script src="bower_components/jquery/dist/jquery.js"></script>
  <script src="bower_components/what-input/dist/what-input.js"></script>
  <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
  <script src="js/app.js"></script>
</body>
