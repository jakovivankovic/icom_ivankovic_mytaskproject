


<?php
require_once 'etc/config.php';
$stmt = $db->prepare('INSERT INTO task (description, created_at, created_by, due_at, assigned_to, priority, status, done_by) VALUES (? ,now(), ?, ?, ?, ?, ?, ?)');

$stmt->execute(
    array(
      $_POST['description'],
      $_POST['created_by'],
      $_POST['due_at'],
      $_POST['assigned_to'],
      $_POST['priority'],
      $_POST['status'],
      $_POST['done_by'],
    ));
header('location:index.php')
?>
