<?php
$db = new PDO('mysql:host=localhost;dbname=task1','root','root');

$sth = $db->prepare("SELECT
  task.id,
  description,
  created_at,
  due_at,
  priority,
  status,
  creator.name as creator_name,
  assignor.name as assignor_name,
  finishor.name as finishor_name
  FROM task
  INNER JOIN user as creator on created_by = creator.id
  INNER JOIN user as assignor on assigned_to = assignor.id
  LEFT JOIN user as finishor on done_by = finishor.id
  ");
  $sth->execute();
  $tasks = $sth->fetchAll();
  ?>
