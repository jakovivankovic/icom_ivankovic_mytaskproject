

  <?php require_once 'etc/config.php'; ?>


  <html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MYtask</title>
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body
  <div class="off-canvas-wrapper">
    <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
      <ul class="menu vertical">
        <li><a href="personal.php">Personal Presentation</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="manual.php">Manual</a></li>
        <li><a href="logout.php">Logout</a></li>


    </div>
    <div class="off-canvas-content" data-off-canvas-content>
      <header>
        <div class="title-bar">
          <div class="title-bar-left">
            <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
            <a href="index.php"><span class="title-bar-title">Task List</span></a>
            <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><img class="title-bar-img" src="img/header.jpeg"></button>
          </div>
        </div>
      </header>

      <div class="about">
        <br>
        <h1>My Task</h1>
        <p>This project aims to manage tasks. We have used multiple technologies to developp this project.</p>
        <p>The main objectives of this project were:</p>
        <p>-Add a task</p>
        <p>-Remove a task</p>
        <p>-Edit a task</p>
        <p>-Add an user</p>
        <p>-Remove an user</p>
        <br><br>


        <h3>Technology used</h3>
        <p>Html, Css, php, mysql, foundation</p>
        <p>June 2017</p>
      </div>

    <footer>
      <div class="small-12 large-12 columns">
        <div class="copyrights">
          <p>&copy; Copyright 2017 Jakov Ivankovic</p>
        </div>
      </div>
    </footer>
  </div>

  <script src="bower_components/jquery/dist/jquery.js"></script>
  <script src="bower_components/what-input/dist/what-input.js"></script>
  <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
  <script src="js/app.js"></script>
  </body>
