<!doctype html>
<?php require_once 'etc/config.php'; ?>

<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MYtask</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <form class="modal-content" action="register.php">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>

    <div class="container">
      <label><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="user" required>

      <label><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="pass" required>
      <button type="submit" class="loginbtn">Login</button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
      <span class="psw"><a href="adduser.php">Register</a></span>
    </div>
  </form>

  <footer>
    <div class="small-12 large-12 columns">
      <div class="copyrights">
        <p>&copy; Copyright 2017 Jakov Ivankovic</p>
      </div>
    </div>
  </footer>
</div>

<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/what-input/dist/what-input.js"></script>
<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
<script src="js/app.js"></script>
</body>
