<!doctype html>

<?php require_once 'etc/config.php'; ?>


<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MYtask</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body
<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    <ul class="menu vertical">
      <li><a href="personal.php">Personal Presentation</a></li>
      <li><a href="about.php">About</a></li>
      <li><a href="manual.php">Manual</a></li>
      <li><a href="adduser.php">Add User</a></li>
      <li><a href="logout.php">Logout</a></li>
      <br><br>
      <li>Order By</li>
      <li><a data-filter="id">id</a></li>
      <li><a data-filter="status">status</a></li>
    </ul>
  </div>

  <div class="off-canvas-content" data-off-canvas-content>
    <header>
      <div class="title-bar">
        <div class="title-bar-left">
          <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
          <a href="index.php"><span class="title-bar-title">Task List</span></a>
          <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><img class="title-bar-img" src="img/header.jpeg"></button>
        </div>
      </div>
    </header>


    <div class="container">
      <main>
        <ul class="tasklist">
          <li class="toplist">
            <span class="tasklist-item-id">ID</span>
            <span class="tasklist-item-description">Description</span>
            <span class="tasklist-item-date">Created At</span>
            <span class="tasklist-item-createdby">Created By</span>
            <span class="tasklist-item-duedate">Due At</span>
            <span class="tasklist-item-assignedto">Assigned To</span>
            <span class="tasklist-item-priority">Priority</span>
            <span class="tasklist-item-status">Status</span>
            <span class="tasklist-item-doneby">Done By</span>
            <span class="emptySpace"></span>
          </li>

          <?php foreach ($tasks as $task): ?>
              <li class="elementlist">
                <span class="tasklist-item-id"><?php echo $task['id']?></span>
                <span class="tasklist-item-description"><?php echo $task['description']?></span>
                <span class="tasklist-item-date"><?php echo $task['created_at'] ?></span>
                <span class="tasklist-item-createdby"><?php echo $task['creator_name'] ?></span>
                <span class="tasklist-item-duedate"><?php echo $task['due_at'] ?></span>
                <span class="tasklist-item-assignedto"><?php echo $task['assignor_name'] ?></span>
                <span class="tasklist-item-priority"><?php echo $task['priority'] ?></span>
                <span class="tasklist-item-status"><?php echo $task['status'] ?></span>
                <span class="tasklist-item-doneby"><?php echo $task['finishor_name'] ?></span>
                <span><a href="#" class="tasklist-item-done"></a></span>
                <span class="tasklist-item-delete"><a href="delete.php?task=<?php echo $task['id'];?>" class="tasklist-item-delete"><img src="img/delete.png"></a></span>
              </li>
          <?php endforeach; ?>
        </ul>
        <a href="add.php" class="btnAdd"><img src="img/add.png"></a>
      </main>
    </div>


    <div id="id01" class="modal">
      <form class="modal-content animate" action="register.php">
        <div class="container">
          <label><b>Username</b></label>
          <input type="text" placeholder="Enter Username" name="user" required>

          <label><b>Password</b></label>
          <input type="password" placeholder="Enter Password" name="psw" required>
          <button type="submit" class="loginbtn">Login</button>
        </div>

        <div class="container" style="background-color:#f1f1f1">
          <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
          <span class="psw"><a href="adduser.php">Register</a></span>
        </div>
      </form>
    </div>
  </div>

  <footer>
    <div class="small-12 large-12 columns">
      <div class="copyrights">
        <p>&copy; Copyright 2017 Jakov Ivankovic</p>
      </div>
    </div>
  </footer>
</div>

<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/what-input/dist/what-input.js"></script>
<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
<script src="js/app.js"></script>
</body>
