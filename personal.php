


<!doctype html>

<?php require_once 'etc/config.php'; ?>


<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>MYtask</title>
  <link rel="stylesheet" href="css/app.css">
</head>
<body
<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    <ul class="menu vertical">
      <li><a href="personal.php">Personal Presentation</a></li>
      <li><a href="about.php">About</a></li>
      <li><a href="manual.php">Manual</a></li>
      <li><a href="adduser.php">Add User</a></li>
      <li><a href="logout.php">Logout</a></li>
      <br><br>
      <li>Order By</li>
      <li><a data-filter="id">id</a></li>
      <li><a data-filter="status">status</a></li>
    </ul>
  </div>

  <div class="off-canvas-content" data-off-canvas-content>
    <header>
      <div class="title-bar">
        <div class="title-bar-left">
          <button class="menu-icon" type="button" data-open="offCanvasLeft"></button>
          <a href="index.php"><span class="title-bar-title">Task List</span></a>
          <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><img class="title-bar-img" src="img/header.jpeg"></button>
        </div>
      </div>
    </header>


    <!-- PRINCIPAL CONTENT  -->
    <section class="row  site-cv ">
      <div class="columns cvtitle">
        <h1 class="cvtitle">Curriculum Vitae</h1>
      </div>
      <div class="columns small-12 medium-12 large-12">
        <table>
          <tr>
            <td><strong>Jakov Ivankovic</strong></td>
            <td><strong>Telecommunication</strong></td>
          </tr>
          <tr>
            <td><strong>Birth Date :</strong></td>
            <td>27 june 1995</td>
            <td><strong>Status :</strong></td>
            <td>Single</td>
          </tr>
          <tr>
            <td><strong>Address :</strong></td>
            <td>1700 Fribourg </td>
            <td><strong>Origin :</strong></td>
            <td>Croatian</td>
          </tr>
          <tr>
            <td><strong>Street :</strong></td>
            <td>Chemin des mésanges 14</td>
            <td><strong>E-mail :</strong></td>
            <td>
              <a href="mailto:jakovivankovic@gmail.com">jakovivankovic@gmail.com</a>
            </td>
          </tr>
        </table>
      </div>


      <div class="columns cvtitle2">
        <h1 class="cvtitle2">Education</h1>
      </div>
      <div class="columns small-12 medium-12 large-12">
        <table>
          <tr>
            <td><strong>2015-2019</strong></td>
            <td><strong>Bachelor in Telecommunication (specialized in Internet and Communication, german-french)</strong><br>School of Engineering Fribourg, Bd de Pérolles 80, 1700 Fribourg, Switzerland</td>
          </tr>
          <tr>
            <td><strong>2010-2015</strong></td>
            <td><strong>Federal Certificate of Capacity in Electronic</strong><br>EPAI Bellinzona, 6500 Bellinzona, Switzerland</td>
          </tr>
        </table>
      </div>



      <div class="columns cvtitle3">
        <h1 class="cvtitle3">Languages</h1>
      </div>
      <div class="columns small-12 medium-12 large-12">
        <table>
          <tr>
            <td><strong>Italian</strong></td>
            <td>Native language 1</td>
            <tr>
              <td><strong>Croatian</strong></td>
              <td>Native language 2</td>
            </tr>
            <tr>
              <td><strong>English</strong></td>
              <td>Intermediate, B2 Level (Common European Framework)</td>
            </tr>
            <tr>
              <td><strong>German</strong></td>
              <td>Intermediate, B2 Level (Common European Framework)</td>
            </tr>
            <tr>
              <td><strong>French</strong></td>
              <td>Intermediate, B2 Level (Common European Framework)</td>
            </tr>
          </table>
        </section>
  </div>

  <footer>
    <div class="small-12 large-12 columns">
      <div class="copyrights">
        <p>&copy; Copyright 2017 Jakov Ivankovic</p>
      </div>
    </div>
  </footer>
</div>

<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/what-input/dist/what-input.js"></script>
<script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
<script src="js/app.js"></script>
</body>
